
# Changelog for Social Networking Library Stubs

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.1.0-SNAPSHOT]

 - updated to maven-portal-bom 4.0.0

## [v1.0.1] - 2024-04-16

 - Null pointer exception bug fixed
 - Removed unnecessary logs

## [v1.0.0] - 2023-11-15

 - First release
