package org.gcube.portal.databook.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;

import org.gcube.portal.databook.shared.*;
import org.gcube.portal.databook.shared.ex.ColumnNameNotFoundException;
import org.gcube.portal.databook.shared.ex.CommentIDNotFoundException;
import org.gcube.portal.databook.shared.ex.FeedIDNotFoundException;
import org.gcube.portal.databook.shared.ex.FeedTypeNotFoundException;
import org.gcube.portal.databook.shared.ex.InviteIDNotFoundException;
import org.gcube.portal.databook.shared.ex.InviteStatusNotFoundException;
import org.gcube.portal.databook.shared.ex.LikeIDNotFoundException;
import org.gcube.portal.databook.shared.ex.NotificationChannelTypeNotFoundException;
import org.gcube.portal.databook.shared.ex.NotificationIDNotFoundException;
import org.gcube.portal.databook.shared.ex.NotificationTypeNotFoundException;
import org.gcube.portal.databook.shared.ex.PrivacyLevelTypeNotFoundException;
import org.gcube.social_networking.social_networking_client_library.LibClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Massimiliano Assante ISTI-CNR
 * @author Costantino Perciante ISTI-CNR
 * This class is used for querying and adding data to Cassandra via Astyanax High Level API 
 */
public final class DBCassandraAstyanaxImpl implements DatabookStore {

	/**
	 * logger
	 */
	private static final Logger _log = LoggerFactory.getLogger(DBCassandraAstyanaxImpl.class);
	private static LibClient libClient;


	/**
	 * use this constructor carefully from test classes
	 * @param dropSchema set true if you want do drop the current and set up new one
	 */
	protected DBCassandraAstyanaxImpl(boolean dropSchema) {
		try {
			libClient = new LibClient();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
	/**
	 * public constructor, no dropping schema is allowed
	 */
	public DBCassandraAstyanaxImpl() {
		try {
			libClient = new LibClient();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * public constructor, no dropping schema is allowed, infrastructureName is given.
	 */
	public DBCassandraAstyanaxImpl(String infrastructureName) {
		try {
			libClient = new LibClient();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 *
	 ********************** 	FRIENDSHIPS (CONNECTIONS)	***********************
	 *
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean requestFriendship(String from, String to) {
		return true;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean approveFriendship(String from, String to) {
		return true;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean denyFriendship(String from, String to) {
		return true;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getFriends(String userid) {
		ArrayList<String> toReturn = new ArrayList<String>();
		return toReturn;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getPendingFriendRequests(String userid) {
		ArrayList<String> toReturn = new ArrayList<String>();
		return toReturn;
	}
	/*
	 *
	 ********************** 	FEEDS	***********************
	 *
	 */

	private static Post feed2post(Feed feed){
		Post post = new Post();
		if (feed.getKey()!=null)post.setKey(feed.getKey());
		if (feed.getType()!=null)post.setType(PostType.valueOf(feed.getType().toString()));
		if (feed.getEntityId()!=null)post.setEntityId(feed.getEntityId());
		if (feed.getTime()!=null)post.setTime(feed.getTime());
		if (feed.getVreid()!=null)post.setVreid(feed.getVreid());
		if (feed.getUri()!=null)post.setUri(feed.getUri());
		if (feed.getUriThumbnail()!=null)post.setUriThumbnail(feed.getUriThumbnail());
		if (feed.getDescription()!=null)post.setDescription(feed.getDescription());
		if (feed.getPrivacy()!=null)post.setPrivacy(feed.getPrivacy());
		if (feed.getFullName()!=null)post.setFullName(feed.getFullName());
		if (feed.getEmail()!=null)post.setEmail(feed.getEmail());
		if (feed.getThumbnailURL()!=null)post.setThumbnailURL(feed.getThumbnailURL());
		if (feed.getCommentsNo()!=null)post.setCommentsNo(feed.getCommentsNo());
		if (feed.getLikesNo()!=null)post.setLikesNo(feed.getLikesNo());
		if (feed.getLinkTitle()!=null)post.setLinkTitle(feed.getLinkTitle());
		if (feed.getLinkDescription()!=null)post.setLinkDescription(feed.getLinkDescription());
		if (feed.getLinkHost()!=null)post.setLinkHost(feed.getLinkHost());
		post.setApplicationFeed(feed.isApplicationFeed());
		post.setMultiFileUpload(feed.isMultiFileUpload());
		return post;
	}

	private static Feed post2feed(Post post){
		Feed feed = new Feed();

		if (post.getKey()!=null)feed.setKey(post.getKey());
		if (post.getType()!=null)feed.setType(FeedType.valueOf(post.getType().toString()));
		if (post.getEntityId()!=null)feed.setEntityId(post.getEntityId());
		if (post.getTime()!=null)feed.setTime(post.getTime());
		if (post.getVreid()!=null)feed.setVreid(post.getVreid());
		if (post.getUri()!=null)feed.setUri(post.getUri());
		if (post.getUriThumbnail()!=null)feed.setUriThumbnail(post.getUriThumbnail());
		if (post.getDescription()!=null)feed.setDescription(post.getDescription());
		if (post.getPrivacy()!=null)feed.setPrivacy(post.getPrivacy());
		if (post.getFullName()!=null)feed.setFullName(post.getFullName());
		if (post.getEmail()!=null)feed.setEmail(post.getEmail());
		if (post.getThumbnailURL()!=null)feed.setThumbnailURL(post.getThumbnailURL());
		if (post.getCommentsNo()!=null)feed.setCommentsNo(post.getCommentsNo());
		if (post.getLikesNo()!=null)feed.setLikesNo(post.getLikesNo());
		if (post.getLinkTitle()!=null)feed.setLinkTitle(post.getLinkTitle());
		if (post.getLinkDescription()!=null)feed.setLinkDescription(post.getLinkDescription());
		if (post.getLinkHost()!=null)feed.setLinkHost(post.getLinkHost());
		feed.setApplicationFeed(post.isApplicationFeed());
		feed.setMultiFileUpload(post.isMultiFileUpload());

		return feed;
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public boolean saveUserFeed(Feed post) {
		return saveUserPost(feed2post(post));
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean saveUserPost(Post post) {
		return libClient.saveUserPostLib(post);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public boolean saveUserFeed(Feed feed, List<Attachment> attachments) {
		return saveUserPost(feed2post(feed), attachments);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean saveUserPost(Post post, List<Attachment> attachments) {
		return libClient.saveUserPostLib(post, attachments);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public boolean saveAppFeed(Feed post) {
		return saveAppPost(feed2post(post));
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean saveAppPost(Post post) {
		return libClient.saveAppPostLib(post);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public boolean saveAppFeed(Feed feed, List<Attachment> attachments) {
		return saveAppPost(feed2post(feed), attachments);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean saveAppPost(Post post, List<Attachment> attachments) {
		return libClient.saveAppPostLib(post, attachments);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public boolean saveFeedToVRETimeline(String feedKey, String vreid) throws FeedIDNotFoundException {
		return savePostToVRETimeline(feedKey, vreid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean savePostToVRETimeline(String postKey, String vreid) throws FeedIDNotFoundException {
		return libClient.savePostToVRETimelineLib(postKey, vreid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public Feed readFeed(String feedid)
			throws PrivacyLevelTypeNotFoundException,
			FeedTypeNotFoundException, FeedIDNotFoundException, ColumnNameNotFoundException {

		return post2feed(readPost(feedid));
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Post readPost(String postid)
			throws PrivacyLevelTypeNotFoundException,
			FeedTypeNotFoundException, FeedIDNotFoundException, ColumnNameNotFoundException {
		return libClient.readPostLib(postid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public List<Feed> getRecentFeedsByUserAndDate(String userid, long timeInMillis) throws IllegalArgumentException {
		Date now = new Date();
		List<Feed> feeds = new ArrayList<>();
		if (timeInMillis > now.getTime())
			throw new IllegalArgumentException("the timeInMillis must be before today");
		List<Post> posts = getRecentPostsByUserAndDate(userid, timeInMillis);
		if(posts!=null){
			_log.debug("Length of posts is " + posts.size());
			for(Post post: posts){
				_log.debug(post.toString());
				feeds.add(post2feed(post));
			}
		}
		
		return feeds;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Post> getRecentPostsByUserAndDate(String userid, long timeInMillis) throws IllegalArgumentException {
		Date now = new Date();
		if (timeInMillis > now.getTime())
			throw new IllegalArgumentException("the timeInMillis must be before today");

		return libClient.getRecentPostsByUserAndDateLib(userid, timeInMillis);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public boolean deleteFeed(String feedId) throws FeedIDNotFoundException, PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException {
		return deletePost(feedId);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean deletePost(String postid) throws FeedIDNotFoundException, PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException {
		return libClient.deletePostLib(postid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public List<Feed> getAllFeedsByUser(String userid) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		List<Post> posts = getAllPostsByUser(userid);
		List<Feed> feeds = new ArrayList<>();
		if(posts!=null){
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		return feeds;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Post> getAllPostsByUser(String userid) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		return libClient.getAllPostsByUserLib(userid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Feed> getAllFeedsByApp(String appid) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		List<Post> posts = getAllPostsByApp(appid);
		List<Feed> feeds = new ArrayList<>();
		if(posts!=null){
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		
		return feeds;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Post> getAllPostsByApp(String appid) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		return libClient.getAllPostsByAppLib(appid);
	}
	/**
	 * {@inheritDoc}
	 * @throws Exception
	 */
	@Deprecated
	@Override
	public List<Feed> getRecentCommentedFeedsByUserAndDate(String userid,
			long timeInMillis) throws Exception {
		List<Post> posts = getRecentCommentedPostsByUserAndDate(userid, timeInMillis);
		List<Feed> feeds = new ArrayList<>();
		if(posts!=null){
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		return feeds;
	}
	/**
	 * {@inheritDoc}
	 * @throws Exception
	 */
	@Override
	public List<Post> getRecentCommentedPostsByUserAndDate(String userid,
			long timeInMillis) throws Exception {
		return libClient.getRecentCommentedPostsByUserAndDateLib(userid, timeInMillis);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public List<Feed> getAllPortalPrivacyLevelFeeds() throws FeedTypeNotFoundException, ColumnNameNotFoundException, PrivacyLevelTypeNotFoundException {
		List<Post> posts = getAllPortalPrivacyLevelPosts();
		List<Feed> feeds = new ArrayList<>();
		if (posts!=null){
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		return feeds;
	}
	@Override
	public List<Post> getAllPortalPrivacyLevelPosts() throws FeedTypeNotFoundException, ColumnNameNotFoundException, PrivacyLevelTypeNotFoundException {
		return libClient.getAllPortalPrivacyLevelPostsLib();
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public List<Feed> getRecentFeedsByUser(String userid, int quantity)	throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		List<Post> posts = getRecentPostsByUser(userid, quantity);
		List<Feed> feeds = new ArrayList<>();
		if(posts!=null){
				for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		return feeds;
	}
	@Override
	public List<Post> getRecentPostsByUser(String userid, int quantity)	throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		return libClient.getRecentPostsByUserLib(userid, quantity);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public List<Feed> getAllFeedsByVRE(String vreid) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		List<Post> posts = getAllPostsByVRE(vreid);
		List<Feed> feeds = new ArrayList<>();
		if(posts!=null){
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		return feeds;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Post> getAllPostsByVRE(String vreid) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		return libClient.getAllPostsByVRELib(vreid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public List<Feed> getRecentFeedsByVRE(String vreid, int quantity) throws PrivacyLevelTypeNotFoundException,	FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		_log.debug("\n\n in getRecentFeedsByVRE");
		List<Post> posts = getRecentPostsByVRE(vreid, quantity);
		List<Feed> feeds = new ArrayList<>();
		if(posts!=null){
			_log.debug("length of vre posts = " + posts.size());
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		_log.debug("length of vre feeds = " + feeds.size());
		return feeds;
	}
	@Override
	public List<Post> getRecentPostsByVRE(String vreid, int quantity) throws PrivacyLevelTypeNotFoundException,	FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		_log.debug("\n\n in getRecentPostsByVRE");
		return libClient.getRecentPostsByVRELib(vreid, quantity);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public RangeFeeds getRecentFeedsByVREAndRange(String vreid, int from, int quantity) throws IllegalArgumentException, PrivacyLevelTypeNotFoundException,	FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		RangePosts rangePosts = getRecentPostsByVREAndRange(vreid, from, quantity);
		List<Post> posts = rangePosts.getPosts();
		if(posts!=null){
			ArrayList<Feed> feeds = new ArrayList<>();
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
			RangeFeeds rangeFeeds = new RangeFeeds(rangePosts.getLastReturnedPostTimelineIndex(), feeds);
			return rangeFeeds;
		}
		else{
			return new RangeFeeds();
		}
		
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public RangePosts getRecentPostsByVREAndRange(String vreid, int from, int quantity) throws IllegalArgumentException, PrivacyLevelTypeNotFoundException,	FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		return libClient.getRecentPostsByVREAndRangeLib(vreid, from, quantity);
	}

	/*
	 *
	 ********************** 	NOTIFICATIONS	***********************
	 *
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean saveNotification(Notification n) {
		return libClient.saveNotificationLib(n);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Notification readNotification(String notificationid) throws NotificationIDNotFoundException, NotificationTypeNotFoundException, ColumnNameNotFoundException {
		return libClient.readNotificationLib(notificationid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setNotificationRead(String notificationidToSet) throws NotificationIDNotFoundException, NotificationTypeNotFoundException, ColumnNameNotFoundException {
		return libClient.setNotificationReadLib(notificationidToSet);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Notification> getAllNotificationByUser(String userid, int limit) throws NotificationTypeNotFoundException,	ColumnNameNotFoundException {
		return libClient.getAllNotificationByUserLib(userid, limit);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Notification> getUnreadNotificationsByUser(String userid) throws NotificationTypeNotFoundException,	ColumnNameNotFoundException, NotificationIDNotFoundException {
		return libClient.getUnreadNotificationsByUserLib(userid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Notification> getRangeNotificationsByUser(String userid,int from, int quantity) throws NotificationTypeNotFoundException, ColumnNameNotFoundException, NotificationIDNotFoundException {
		return libClient.getRangeNotificationsByUserLib(userid, from, quantity);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setAllNotificationReadByUser(String userid) throws NotificationTypeNotFoundException, ColumnNameNotFoundException {
		return libClient.setAllNotificationReadByUserLib(userid);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkUnreadNotifications(String userid) throws NotificationTypeNotFoundException, ColumnNameNotFoundException {
		return libClient.checkUnreadNotificationsLib(userid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkUnreadMessagesNotifications(String userid) throws NotificationIDNotFoundException, NotificationTypeNotFoundException, ColumnNameNotFoundException {
		return libClient.checkUnreadMessagesNotificationsLib(userid);
	}
	/*
	 *
	 ********************** 	NOTIFICATION SETTINGS	***********************
	 *
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NotificationChannelType> getUserNotificationChannels(String userid, NotificationType notificationType) throws NotificationChannelTypeNotFoundException, NotificationTypeNotFoundException {
		return libClient.getUserNotificationChannelsLib(userid, notificationType);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setUserNotificationPreferences(String userid, Map<NotificationType, NotificationChannelType[]> enabledChannels) {
		if(enabledChannels!=null){
			for(NotificationType notificationType: enabledChannels.keySet()){
				_log.debug("Type: " + notificationType.toString());
				for(NotificationChannelType channelType: enabledChannels.get(notificationType)){
					_log.debug(channelType.toString());
				}
			}
		}
		return libClient.setUserNotificationPreferencesLib(userid, enabledChannels);
	}
	/**
	 * {@inheritDoc}
	 *
	 * by default Workspace and Calendar Notifications are set to Portal
	 */
	@Override
	public Map<NotificationType, NotificationChannelType[]> getUserNotificationPreferences(String userid) throws NotificationTypeNotFoundException, NotificationChannelTypeNotFoundException {
		return libClient.getUserNotificationPreferencesLib(userid);
	}
	/*
	 *
	 ********************** 	COMMENTS	***********************
	 *
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addComment(Comment comment) throws FeedIDNotFoundException {
		return libClient.addCommentLib(comment)!=null;
	}
	/**
	 * {@inheritDoc}
	 */
	public Comment readCommentById(String commentId) throws CommentIDNotFoundException {
		return libClient.readCommentByIdLib(commentId);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public List<Comment> getAllCommentByFeed(String feedid) {
		return getAllCommentByPost(feedid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Comment> getAllCommentByPost(String postid) {
		return libClient.getAllCommentsByPostIdLib(postid);
	}

	/**
	 * {@inheritDoc}
	 * @throws Exception
	 */
	@Override
	public List<Comment> getRecentCommentsByUserAndDate(final String userid,
			final long timeInMillis) throws Exception {

		return libClient.getRecentCommentsByUserAndDateLib(userid, timeInMillis);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean editComment(Comment comment2Edit) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException,	CommentIDNotFoundException, FeedIDNotFoundException {
		return libClient.editCommentLib(comment2Edit)!=null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean deleteComment(String commentid, String feedid) throws PrivacyLevelTypeNotFoundException,	FeedTypeNotFoundException, ColumnNameNotFoundException,	CommentIDNotFoundException, FeedIDNotFoundException {
		return libClient.deleteCommentLib(commentid, feedid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean like(Like like) throws FeedIDNotFoundException {
		return libClient.likeLib(like);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unlike(String userid, String likeid, String feedid) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, LikeIDNotFoundException, FeedIDNotFoundException {
		return libClient.unlikeLib(userid, likeid, feedid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public List<String> getAllLikedFeedIdsByUser(String userid) {
		return getAllLikedPostIdsByUser(userid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getAllLikedPostIdsByUser(String userid) {
		return libClient.getAllLikedPostIdsByUserLib(userid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Feed> getAllLikedFeedsByUser(String userid, int limit) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		ArrayList<Feed> toReturn = new ArrayList<>();
		List<String> likedPostIDs = getAllLikedPostIdsByUser(userid);

		if(likedPostIDs!=null){
			//check if quantity is greater than user feeds
			limit = (limit > likedPostIDs.size()) ? likedPostIDs.size() : limit;

			//need them in reverse order
			for (int i = likedPostIDs.size()-1; i >= (likedPostIDs.size()-limit); i--) {
				Feed toAdd = readFeed(likedPostIDs.get(i));
				if (toAdd.getType() == FeedType.TWEET || toAdd.getType() == FeedType.SHARE || toAdd.getType() == FeedType.PUBLISH) {
					toReturn.add(toAdd);
					_log.trace("Read recent post: " + likedPostIDs.get(i));
				} else {
					_log.trace("Read and skipped post: " + likedPostIDs.get(i) + " (Removed post)");
					limit += 1; //increase the quantity in case of removed feed
					//check if quantity is greater than user feeds
					limit = (limit > likedPostIDs.size()) ? likedPostIDs.size() : limit;
				}
			}

		}
		
		return toReturn;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Post> getAllLikedPostsByUser(String userid, int limit) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, ColumnNameNotFoundException, FeedIDNotFoundException {
		ArrayList<Post> toReturn = new ArrayList<Post>();
		List<String> likedPostIDs = getAllLikedPostIdsByUser(userid);

		if(likedPostIDs!=null){
			//check if quantity is greater than user feeds
			limit = (limit > likedPostIDs.size()) ? likedPostIDs.size() : limit;

			//need them in reverse order
			for (int i = likedPostIDs.size()-1; i >= (likedPostIDs.size()-limit); i--) {
				Post toAdd = readPost(likedPostIDs.get(i));
				if (toAdd.getType() == PostType.TWEET || toAdd.getType() == PostType.SHARE || toAdd.getType() == PostType.PUBLISH) {
					toReturn.add(toAdd);
					_log.trace("Read recent post: " + likedPostIDs.get(i));
				} else {
					_log.trace("Read and skipped post: " + likedPostIDs.get(i) + " (Removed post)");
					limit += 1; //increase the quantity in case of removed feed
					//check if quantity is greater than user feeds
					limit = (limit > likedPostIDs.size()) ? likedPostIDs.size() : limit;
				}
			}
		}
		
		return toReturn;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Feed> getRecentLikedFeedsByUserAndDate(String userid,
			long timeInMillis) throws IllegalArgumentException {

		List<Feed> toReturn = new ArrayList<>();

		Date now = new Date();
		if (timeInMillis > now.getTime())
			throw new IllegalArgumentException("the timeInMillis must be before today");

		if(userid == null || userid.isEmpty())
			throw new IllegalArgumentException("the userId parameter cannot be null/empty");

		// get the list of liked feeds
		List<String> likedPostsIdsByUser = getAllLikedFeedIdsByUser(userid);

		if(likedPostsIdsByUser != null && !likedPostsIdsByUser.isEmpty()){
			for(int i = likedPostsIdsByUser.size() - 1; i >= 0; i--){
				String postid = likedPostsIdsByUser.get(i);
				try{

					// retrieve the Post
					Feed toCheck = readFeed(postid);
					boolean isPostOk = (toCheck.getType() == FeedType.TWEET || toCheck.getType() == FeedType.SHARE || toCheck.getType() == FeedType.PUBLISH);

					// retrieve the like of the user for the post
					if(isPostOk){
						List<Like> likes = getAllLikesByFeed(postid);
						for (Like like : likes) {
							if(like.getTime().getTime() >= timeInMillis && like.getUserid().equals(userid))
								toReturn.add(toCheck);
						}
					}

				}catch(Exception e){
					_log.error("Skipped post with id " + postid, e);
				}
			}
		}

		// please check consider that if a user made like recently to an old post, well it could happen that this
		// post comes first than a newer post in the toReturn list. Thus we need to sort it.
		Collections.sort(toReturn, Collections.reverseOrder());

		return toReturn;

	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Post> getRecentLikedPostsByUserAndDate(String userid,
			long timeInMillis) throws IllegalArgumentException {

		List<Post> toReturn = new ArrayList<>();

		Date now = new Date();
		if (timeInMillis > now.getTime())
			throw new IllegalArgumentException("the timeInMillis must be before today");

		if(userid == null || userid.isEmpty())
			throw new IllegalArgumentException("the userId parameter cannot be null/empty");

		// get the list of liked feeds
		List<String> likedPostsIdsByUser = getAllLikedPostIdsByUser(userid);

		if(likedPostsIdsByUser != null && !likedPostsIdsByUser.isEmpty()){
			for(int i = likedPostsIdsByUser.size() - 1; i >= 0; i--){
				String postid = likedPostsIdsByUser.get(i);
				try{

					// retrieve the Post
					Post toCheck = readPost(postid);
					boolean isPostOk = (toCheck.getType() == PostType.TWEET || toCheck.getType() == PostType.SHARE || toCheck.getType() == PostType.PUBLISH);

					// retrieve the like of the user for the post
					if(isPostOk){
						List<Like> likes = getAllLikesByPost(postid);
						for (Like like : likes) {
							if(like.getTime().getTime() >= timeInMillis && like.getUserid().equals(userid))
								toReturn.add(toCheck);
						}
					}

				}catch(Exception e){
					_log.error("Skipped post with id " + postid, e);
				}
			}
		}

		// please check consider that if a user made like recently to an old post, well it could happen that this
		// post comes first than a newer post in the toReturn list. Thus we need to sort it.
		Collections.sort(toReturn, Collections.reverseOrder());

		return toReturn;
	}

	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public List<Like> getAllLikesByFeed(String feedid) {
		return getAllLikesByPost(feedid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Like> getAllLikesByPost(String postid) {
		//possible error index
		return libClient.getAllLikesByPostLib(postid);
	}
	/*
	 *
	 ********************** 	HASHTAGS	***********************
	 *
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean saveHashTags(String feedid, String vreid, List<String> hashtags)	throws FeedIDNotFoundException {
		return libClient.saveHashTagsLib(feedid, vreid, hashtags);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean deleteHashTags(String feedid, String vreid, List<String> hashtags) throws FeedIDNotFoundException {
		return libClient.deleteHashTagsLib(feedid, vreid, hashtags);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean saveHashTagsComment(String commentId, String vreid, List<String> hashtags)	throws CommentIDNotFoundException {
		return libClient.saveHashTagsCommentLib(commentId, vreid, hashtags);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean deleteHashTagsComment(String commentId, String vreid, List<String> hashtags) throws CommentIDNotFoundException {
		return libClient.deleteHashTagsCommentLib(commentId, vreid, hashtags);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Integer> getVREHashtagsWithOccurrence(String vreid) {
		return libClient.getVREHashtagsWithOccurrenceLib(vreid);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Integer> getVREHashtagsWithOccurrenceFilteredByTime(String vreid, long timestamp){
		return libClient.getVREHashtagsWithOccurrenceFilteredByTimeLib(vreid, timestamp);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Feed> getVREFeedsByHashtag(String vreid, String hashtag) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, FeedIDNotFoundException, ColumnNameNotFoundException {
		List<Post>posts = getVREPostsByHashtag(vreid,hashtag);
		List<Feed>feeds = new ArrayList<>();
		if(posts!=null){
			for(Post post: posts){
				feeds.add(post2feed(post));
			}
		}
		
		return feeds;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Post> getVREPostsByHashtag(String vreid, String hashtag) throws PrivacyLevelTypeNotFoundException, FeedTypeNotFoundException, FeedIDNotFoundException, ColumnNameNotFoundException {
		return libClient.getVREPostsByHashtagLib(vreid, hashtag);
	}
	/*
	 *
	 ********************** 	Invites	***********************
	 *
	 */


	/**
	 * {@inheritDoc}
	 */
	@Override
	public String isExistingInvite(String vreid, String email) {
		return libClient.isExistingInviteLib(vreid, email);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public InviteOperationResult saveInvite(Invite invite) throws AddressException {
		return libClient.saveInviteLib(invite);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Invite readInvite(String inviteid) throws InviteIDNotFoundException, InviteStatusNotFoundException {
		return libClient.readInviteLib(inviteid);
	}

	/**
	 * {@inheritDoc}
	 * @throws InviteStatusNotFoundException
	 */
	@Override
	public boolean setInviteStatus(String vreid, String email, InviteStatus status) throws InviteIDNotFoundException, InviteStatusNotFoundException {
		return libClient.setInviteStatusLib(vreid, email, status);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Invite> getInvitedEmailsByVRE(String vreid, InviteStatus... status) throws InviteIDNotFoundException, InviteStatusNotFoundException{
		return libClient.getInvitedEmailsByVRELib(vreid, status);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Attachment> getAttachmentsByFeedId(String feedId) throws FeedIDNotFoundException {
		return libClient.getAttachmentsByFeedIdLib(feedId);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeConnection() {
	}

	@Override
	public List<String> getAllVREIds(){
		return libClient.getAllVREIdsLib();
	}
}
