package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class CommentIDNotFoundException extends Exception {
	public CommentIDNotFoundException(String message) {
		super(message);
	}
}