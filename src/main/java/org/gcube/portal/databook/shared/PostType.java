package org.gcube.portal.databook.shared;

/**
 * @author Massimiliano Assante ISTI-CNR
 * 
 */
public enum PostType {
	JOIN, SHARE, PUBLISH, TWEET, CONNECTED, 
	/**
	 * Special case used when accounting
	 */
	ACCOUNTING, 
	/**
	 * Special case used when a Feed is removed
	 */
	DISABLED;
}

