package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class ColumnNameNotFoundException extends Exception {
	 public ColumnNameNotFoundException(String message) {
	    super(message);
	  }
}
