package org.gcube.portal.databook.shared;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author Massimiliano Assante, ISTI-CNR
 *
 */
@SuppressWarnings("serial")
public class RangePosts implements Serializable {
	
	private int lastReturnedPostTimelineIndex;
	private ArrayList<Post> posts;
	
	public RangePosts() {
		super();
	}
	
	public RangePosts(int lastReturnedPostTimelineIndex, ArrayList<Post> feeds) {
		super();
		this.lastReturnedPostTimelineIndex = lastReturnedPostTimelineIndex;
		this.posts = feeds;
	}
	
	public int getLastReturnedPostTimelineIndex() {
		return lastReturnedPostTimelineIndex;
	}
	public void setLastReturnedPostTimelineIndex(int lastReturnedPostTimelineIndex) {
		this.lastReturnedPostTimelineIndex = lastReturnedPostTimelineIndex;
	}
	public ArrayList<Post> getPosts() {
		return posts;
	}
	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}
	
	
}
