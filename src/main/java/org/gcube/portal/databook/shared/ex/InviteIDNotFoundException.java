package org.gcube.portal.databook.shared.ex;

@SuppressWarnings("serial")
public class InviteIDNotFoundException extends Exception {
	public InviteIDNotFoundException(String message) {
		super(message);
	}
}