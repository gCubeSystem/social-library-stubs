package org.gcube.portal.databook.shared.ex;


@SuppressWarnings("serial")
public class FeedIDNotFoundException extends Exception {
	public FeedIDNotFoundException(String message, String postId) {
		super("The Post having id: " + postId + " is not present in the database: " + message);
	}
}